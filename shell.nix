with import <nixpkgs> {}; let
  pg-container-name = "spring-postgres-db";
  pg-up = writeShellScriptBin "pg-up" ''
    exists=$(sudo podman ps -a | grep ${pg-container-name})
    if [ -z "$exists" ]; then
      sudo podman run --name ${pg-container-name} -e POSTGRES_PASSWORD=sup3rp@ssword -p 5432:5432 -d postgres
    else
      sudo podman start ${pg-container-name}
    fi
  '';
  pg-down = writeShellScriptBin "pg-down" ''
    sudo podman stop ${pg-container-name}
  '';
in mkShell {
  buildInputs = [
    jdk11
    jetbrains.idea-community
    postgresql
    pg-up pg-down
  ];
}
